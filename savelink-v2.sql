CREATE DEFINER=`root`@`%` PROCEDURE `SaveLink`( in url varchar(245), in description varchar(45) )
BEGIN
	SET @tagToSave = tag;
    
	if exists( select 1 from link where link_name = url ) then
    begin
		update link set link_name=url, link_description=description where link.link_name=url;
	end;
	else
		insert into link (link_name, link_description) values( url, description );
        insert into linkToMetatag ( tag_name, link_id ) 
			select @tagToSave, link_id from link where link.link_name = url;
	end if;
END